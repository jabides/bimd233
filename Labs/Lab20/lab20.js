
$(document).ready(function() {
    function Search(query) {
        this.query = query;
        this.url = 'https://newsapi.org/v2/everything?' +
        'q=' + query + '&' +
        'sortBy=popularity&' +
        'apiKey=e485abf37a6e42178481d2eaee3dfd18';
    };
    
    $(".search").on("click", function() {
        if ($("input").val() == "") return;
        var searchRequest = new Search($("input").val());
    
        var req = new Request(searchRequest.url);
        
        var results = null;
        
        fetch(req)
            .then(function(response) {
                return response.json();
            })
            .then(function(results) {
                
                var el = document.getElementById("newslist");
                el.innerHTML = "";
                
                for (var i = 0; i < results.articles.length; i++) {
                    el.innerHTML += "<div class=\"post-preview\">" + 
                                        "<img class=\"img-responsive\" src=\"" + results.articles[i]['urlToImage'] + "\" height=\"400\">" +
                                        "<a href=\"" + results.articles[i]['url'] + "\">" + 
                                        "<h2 class=\"post-title\">" + results.articles[i]['title'] + "</h2>" +
                                        "<h3 class=\"post-subtitle\">" + results.articles[i]['description'] + "</h3>" + 
                                        "</a>" +
                                        "<p class=\"post-meta\"> Source: " +
                                        "<a href=\"" + results.articles[i]['source']['name'] + "\">" + results.articles[i]['source']['name'] + "</a>" +
                                        " published at " + results.articles[i]['publishedAt'] + "</p>" +
                                        "</div>";
                                        
                }
                
                el = document.getElementById("pager");
                el.innerHTML = "<p>Total search results: " + results.totalResults + "</p>";
                
            })
    })
});
