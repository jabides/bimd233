$(document).ready(function () {
  $('li').css('margin', '10px');
  $('li').attr('id', 'uw');

  $('#p1 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(2000, function () {
      console.log("fadeout complete!")
    });
  });

  $('#p2 li').click(function () {
    console.log("$(this):" + $(this));
    this.style.display = "none";
    $(this).fadeIn(2000, function () {
      console.log("fadein complete!")
    });
  });

  $('#p3 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeTo(2000, 0.15, function () {
      console.log("fadeto complete!")
    });
  });


  $('#p4 li:first-child').click(function () {
    console.log("$(this):" + $(this));
    $('#p4 .fadeToggle').fadeToggle(2000, "swing", function () {
      console.log("fadetaoggle complete!")
    });
  });


});