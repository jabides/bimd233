var cars = [ ['Acura', 'TL 3.2', '2001', '$2000', 'Gold'], 
                ['Toyota', 'Prius', '2008', '$5000', 'Grey']];

for (i = 0; i < 2; i++) {
    var temp = document.getElementById("make" + (i + 1));
    temp.textContent = cars[i][0];

    temp = document.getElementById("model" + (i + 1));
    temp.textContent = cars[i][1];

    temp = document.getElementById("year" + (i + 1));
    temp.textContent = cars[i][2];

    temp = document.getElementById("price" + (i + 1));
    temp.textContent = cars[i][3];

    temp = document.getElementById("color" + (i + 1));
    temp.textContent = cars[i][4];
}