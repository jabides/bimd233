function calcCircleGeometries(radius) {
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
}

var radius1 = prompt("Enter radius 1:");
var radius2 = prompt("Enter radius 2:");
var radius3 = prompt("Enter radius 3:");
var radi = [radius1, radius2, radius3];

for (i = 0; i < 3; i++) {
    var geometries = calcCircleGeometries(radi[i]);
    var temp = document.getElementById("radius" + (i + 1));
    temp.textContent = radi[i];

    temp = document.getElementById("area" + (i + 1));
    temp.textContent = geometries[0];

    temp = document.getElementById("circumference" + (i + 1));
    temp.textContent = geometries[1];
    
    temp = document.getElementById("diameter" + (i + 1));
    temp.textContent = geometries[2];
}

