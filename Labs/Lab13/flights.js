function Flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
    this.airline = airline;
    this.number = number;
    this.origin = origin;
    this.destination = destination;
    this.dep_time = new Date(dep_time);
    this.arrival_time = new Date(arrival_time);
    this.arrival_gate = arrival_gate;
    this.getDuration = function() {
        return (this.arrival_time.getTime() - this.dep_time.getTime()) / 60000;
    }
};

var flight1 = new Flight("Alaska", "ASA1029", "John F Kennedy Intl (KJFK)", "San Francisco (KSFO)", "May 17, 2018 20:22:00", "May 18, 2018 00:02:00", "12");
var flight2 = new Flight("Alaska", "ASA1047", "San Francisco Intl (KSFO)", "Kahului (OGG / PHOG)", "May 17, 2018 19:23:00", "May 17, 2018 21:32:00", "13");
var flight3 = new Flight("Alaska", "ASA1050", "Daniel K Inouye Intl (PHNL)", "San Francisco Intl (KSFO)", "May 17, 2018 14:33:00", "May 17, 2018 22:43:00", "16");
var flight4 = new Flight("Alaska", "ASA1051", "San Francisco Intl (KSFO)", "Daniel K Inouye Intl (PHNL)", "May 17, 2018 18:05:00", "May 17, 2018 20:23:00", "59A");
var flight5 = new Flight("Alaska", "ASA109", "Seattle-Tacoma Intl (KSEA)", "Anchorage Intl (PANC)", "May 17, 2018 19:40:00", "May 17, 2018 22:20:00", "N7");
var flights = [flight1, flight2, flight3, flight4, flight5];

var el = document.getElementById('tbody');
var i;
for (i = 0; i < flights.length; i++) {
    el.innerHTML += "<tr> <td>" + flights[i].airline + "</td>" +
                         "<td>" + flights[i].number + "</td>" + 
                         "<td>" + flights[i].origin + "</td>" +
                         "<td>" + flights[i].destination + "</td>" +
                         "<td>" + flights[i].dep_time + "</td>" +
                         "<td>" + flights[i].arrival_time + "</td>" +
                         "<td>" + flights[i].arrival_gate + "</td>" +
                         "<td>" + flights[i].getDuration() + " min</td>" +
                   "</tr>";
}