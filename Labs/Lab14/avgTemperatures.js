var wx_data = [  {    day: "fri",    hi: 82,    lo: 55  },  {    day: "sat",    hi: 75,    lo: 52  },  {    day: "sun",    hi: 69,    lo: 52  },  {    day: "mon",    hi: 69,    lo: 48  },  {    day: "tue",    hi: 68,    lo: 51  }];


var el = document.getElementById('tbody');
function sum(total, val) {
    return total + val;
}

function getAvg() {
    var highs = [];
    var lows = [];
    for (var i = 0; i < wx_data.length; i++) {
        highs.push(wx_data[i].hi);
        lows.push(wx_data[i].lo);
    }
    var retVal = new Array();
    retVal.push(highs.reduce(sum) / highs.length, lows.reduce(sum) / lows.length);
    return retVal;
}

var i;
for (i = 0; i < wx_data.length; i++) {
    el.innerHTML += "<tr> <td style=\"text-transform:uppercase\">" + wx_data[i].day + "</td>" +
                         "<td>" + wx_data[i].hi + "/" + "<i style=\"color:grey;font-size:85%\">" + wx_data[i].lo + "</i>";
    el.innerHTML += "</tr>";
}

var averages = getAvg();
el = document.getElementById('avgHigh');
el.textContent += " " + averages[0];
el = document.getElementById('avgLow');
el.textContent += " " + averages[1];
el = document.getElementById('avgOverall');
el.textContent += " " + ((averages[0] + averages[1]) / 2).toFixed(1);



