var carName = prompt("Enter car name");
var date = prompt("Enter date");
var miles = prompt("Enter miles driven");

var money = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 2});
var output = document.getElementById("output");
output.textContent = "Car Name: " + carName +
                     "\nDate: " + date +
                     "\nMiles Driven: " + miles +
                     "\nTax Deduction: " + money.format(miles * 0.57);