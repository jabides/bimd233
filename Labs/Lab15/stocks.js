var stock = ["Microsoft", "$381.7 B", "$86.8 B", "$22.1 B", "128,000"];

function Company(name, cap, sales, profit, employees) {
    this.name = name;
    this.cap = cap;
    this.sales = sales;
    this.profit = profit;
    this.employees = employees;
};

var companies = new Array;
companies.push(new Company("Microsoft", "$381.7B", "$86.8B", "$22.1B", 128000));
companies.push(new Company("Symetra Financial", "$2.7B", "$2.2B", "$254.4M", 1400));
companies.push(new Company("Micron Technology", "$37.6B", "$16.4B", "$3.0B", 30400));
companies.push(new Company("F5 Networks", "$9.5B", "$1.7B", "$311.2M", 3834));
companies.push(new Company("Expedia", "$10.8B", "$5.8B", "$398.1M", 18210));
companies.push(new Company("Nautilus", "$476M", "$274.4M", "$18.8M", 340));
companies.push(new Company("Heritage Financial", "$531M", "$137.6M", "$21M", 748));
companies.push(new Company("Cascade Microtech", "$239M", "$136M", "$9.9M", 449));
companies.push(new Company("Nike", "$83.1B", "$27.8B", "2.7B", 56500));
companies.push(new Company("Alaska Air Group", "$7.9B", "$5.4B", "$605M", 13952));

function dump() {
    var el = document.getElementById('tbody');
    el.innerHTML = "";
    companies.forEach(function(company) {
        el.innerHTML += "<tr> <td>" + company.name + "</td>" +
                             "<td>" + company.cap + "</td>" + 
                             "<td>" + company.sales + "</td>" +
                             "<td>" + company.profit + "</td>" +
                             "<td>" + company.employees + "</td>" +
                        "</tr>";
    });
}
