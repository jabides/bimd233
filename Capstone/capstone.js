
$(document).ready(function() {
    var audio = document.getElementById("audio");
    audio.volume = 0.5;
    audio.play();

    $(".fixed").mouseover(function() {
        $(".fixed").css('opacity', 1.0);
    });
    $(".fixed").mouseout(function() {
        $(".fixed").css('opacity', 0.3);
    });
    
    function checkOffset() {
        if($('.fixed').offset().top + $('.fixed').height() >= $('footer').offset().top - 10)
            $('.fixed').css('position', 'absolute');
        if($(document).scrollTop() + window.innerHeight < $('footer').offset().top)
            $('.fixed').css('position', 'fixed'); // restore when you scroll up
    }

    $(document).scroll(function() {
        checkOffset();
    });

    
});
